const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
require('dotenv').config();

const routes = require('./routes/index');

const app = express();
const mongoose = require('mongoose');

// database connection
mongoose.connect(process.env.DATABASE, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
});
mongoose.Promise = global.Promise;
mongoose.connection
  .on('connected', () => {
    console.log(`Mongoose connection open on ${process.env.DATABASE}`);
  })
  .on('error', (err) => {
    console.log(`Connection error: ${err.message}`);
    process.exit(-1);
  });
//-------------------

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use('/', routes);

let PORT = 3000;
if (process.env.NODE_ENV === 'prod') {
  PORT = 8080
}

const server = app.listen(PORT, () => {
    console.log(`Express is running on port ${server.address().port}`);
});